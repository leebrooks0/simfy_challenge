from dog_api.extensions import db


class Dog(db.Model):
    """Basic dog model
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    breed = db.Column(db.String(80), nullable=False)

    def __str__(self):
        return self.name
