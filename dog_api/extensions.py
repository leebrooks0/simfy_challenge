"""Extensions registry

All extensions here are used as singletons and
initialized in application factory
"""
from flask_caching import Cache
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate


cache = Cache(config={'CACHE_TYPE': 'simple'})
db = SQLAlchemy()
ma = Marshmallow()
migrate = Migrate()
