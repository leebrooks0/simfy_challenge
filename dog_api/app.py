from flask import Flask

from dog_api import api
from dog_api.extensions import cache, db, migrate


def create_app(config=None, testing=False, cli=False):
    """Application factory, used to create application
    """
    app = Flask('dog_api')

    configure_app(app, testing)
    configure_extensions(app, cli)
    register_blueprints(app)

    return app


def configure_app(app, testing=False):
    """set configuration for application
    """
    # default configuration
    app.config.from_object('dog_api.config.dev')

    if testing is True:
        # override with testing config
        app.config.from_object('dog_api.config.test')
    else:
        # override with env variable, fail silently if not set
        app.config.from_envvar("DOG_API_CONFIG", silent=True)


def configure_extensions(app, cli):
    """configure flask extensions
    """
    cache.init_app(app)
    db.init_app(app)

    if cli is True:
        migrate.init_app(app, db)


def register_blueprints(app):
    """register all blueprints for application
    """
    app.register_blueprint(api.views.blueprint)
