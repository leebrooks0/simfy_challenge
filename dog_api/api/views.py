from flask import Blueprint
from flask_restful import Api

from dog_api.api.resources import DogResource, DogList


blueprint = Blueprint('api', __name__, url_prefix='/api/v1')
api = Api(blueprint)


api.add_resource(DogResource, '/dogs/<int:dog_id>')
api.add_resource(DogList, '/dogs')
