from .dog import DogResource, DogList


__all__ = [
    'DogResource',
    'DogList',
]
