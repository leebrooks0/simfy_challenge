from flask import request
from flask_restful import Resource

from dog_api.models import Dog
from dog_api.extensions import cache, db, ma
from dog_api.commons.pagination import paginate


class DogSchema(ma.ModelSchema):

    class Meta:
        model = Dog
        sqla_session = db.session


class DogResource(Resource):
    """Single object resource
    """

    @cache.cached()
    def get(self, dog_id):
        schema = DogSchema()
        dog = Dog.query.get_or_404(dog_id)
        return {"dog": schema.dump(dog).data}

    def put(self, dog_id):
        schema = DogSchema(partial=True)
        dog = Dog.query.get_or_404(dog_id)
        dog, errors = schema.load(request.json, instance=dog)
        if errors:
            return ({'errors': errors}, 422)

        db.session.commit()

        return {"msg": "dog updated", "dog": schema.dump(dog).data}

    def delete(self, dog_id):
        dog = Dog.query.get_or_404(dog_id)
        db.session.delete(dog)
        db.session.commit()

        return {"msg": "dog deleted"}


class DogList(Resource):
    """Creation and get_all
    """

    def get(self):
        schema = DogSchema(many=True)
        query = Dog.query
        return paginate(query, schema)

    def post(self):
        schema = DogSchema()
        dog, errors = schema.load(request.json)
        if errors:
            return ({'errors': errors}, 422)

        db.session.add(dog)
        db.session.commit()

        return {"msg": "dog created", "dog": schema.dump(dog).data}, 201
