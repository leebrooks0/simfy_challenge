"""Default configuration

Use env var to override
"""
DEBUG = True
SECRET_KEY = "changeme"

CACHE_DEFAULT_TIMEOUT = 90

SQLALCHEMY_DATABASE_URI = "sqlite:////tmp/dog_api.db"
SQLALCHEMY_TRACK_MODIFICATIONS = False
