TESTING = True  # This makes Flask-Caching use the NullCache for tests

SQLALCHEMY_DATABASE_URI = "sqlite:///:memory:"
CACHE_TIMEOUT = 0
