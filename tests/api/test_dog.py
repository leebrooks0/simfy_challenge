import factory
from pytest_factoryboy import register

from dog_api.models import Dog


@register
class DogFactory(factory.Factory):

    name = factory.Sequence(lambda n: f'name {n}')
    breed = factory.Sequence(lambda n: f'breed {n}')

    class Meta:
        model = Dog


class TestDogResource:

    def test_get_dog(self, client, db, dog):
        # test 404
        rep = client.get("/api/v1/dogs/100000")
        assert rep.status_code == 404

        db.session.add(dog)
        db.session.commit()

        # test get dog
        rep = client.get(f'/api/v1/dogs/{dog.id}')
        assert rep.status_code == 200

        data = rep.get_json()['dog']
        assert data['id'] == dog.id
        assert data['name'] == dog.name
        assert data['breed'] == dog.breed


    def test_put_dog(self, client, db, dog):
        # test 404
        rep = client.put("/api/v1/dogs/100000")
        assert rep.status_code == 404

        db.session.add(dog)
        db.session.commit()

        # test invalid data
        bad_data = {
            'name': None,  # Required
            'breed': 'x' * 81,  # Max length exceeded
        }

        rep = client.put(f'/api/v1/dogs/{dog.id}', json=bad_data)
        assert rep.status_code == 422

        errors = rep.get_json()['errors']
        assert len(errors) == 2
        assert errors['name'][0] == 'Field may not be null.'
        assert errors['breed'][0] == 'Longer than maximum length 80.'

        good_data = {
            'name': 'new_name',
            'breed': 'new_breed',
        }

        # test update dog
        rep = client.put(f'/api/v1/dogs/{dog.id}', json=good_data)
        assert rep.status_code == 200

        data = rep.get_json()['dog']
        assert data['name'] == 'new_name'
        assert data['breed'] == 'new_breed'

        db.session.close()
        dog = db.session.query(Dog).filter_by(id=data['id']).first()
        assert dog.name == 'new_name'
        assert dog.breed == 'new_breed'

    def test_delete_dog(self, client, db, dog):
        # test 404
        rep = client.delete("/api/v1/dogs/100000")
        assert rep.status_code == 404

        db.session.add(dog)
        db.session.commit()

        # test delete dog
        rep = client.delete(f'/api/v1/dogs/{dog.id}')

        assert rep.status_code == 200
        assert db.session.query(Dog).filter_by(id=dog.id).first() is None


class TestDogList:

    def test_create_dog(self, client, db):
        # test invalid data
        bad_data = {
            'name': 'x' * 81,  # Max length exceeded
            'breed': None,  # Required
        }
        rep = client.post('/api/v1/dogs', json=bad_data)

        assert rep.status_code == 422
        errors = rep.get_json()['errors']
        assert len(errors) == 2
        assert errors['name'][0] == 'Longer than maximum length 80.'
        assert errors['breed'][0] == 'Field may not be null.'

        good_data = {
            'name': 'new_name',
            'breed': 'new_breed',
        }

        rep = client.post('/api/v1/dogs', json=good_data)
        assert rep.status_code == 201

        data = rep.get_json()['dog']
        assert data['name'] == 'new_name'
        assert data['breed'] == 'new_breed'

        dog = db.session.query(Dog).filter_by(id=data['id']).first()
        assert dog.name == 'new_name'
        assert dog.breed == 'new_breed'


    def test_get_all_dogs(self, client, db, dog_factory):
        dogs = dog_factory.create_batch(10)

        db.session.add_all(dogs)
        db.session.commit()

        rep = client.get('/api/v1/dogs')
        assert rep.status_code == 200

        results = rep.get_json()
        for dogs in dogs:
            assert any(u['id'] == dogs.id for u in results['results'])
