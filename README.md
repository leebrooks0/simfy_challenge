# Dog API

## Libraries Used
- Cookiecutter Flask Restful - A template for Flask Restful that sets up an ORM (SQLAlchemy),
and test runner (PyTest). Also provides structure and suggested naming etc. out of the box,
allowing the developer to get up and running quickly.
- SQLAlchemy - ORM
- Flask Marshmallow - For serialising/deserialising ORM data
- Flask Caching - Caching library with support for multiple backends
- PyTest - Test runner
- Factory Boy - Easy generation of test data

Other libraries not mentioned here either came with the template and were not relevant
to this particular challenge, or provided extra functionality for the above libraries
e.g. extra PyTest utils for Flask

## Technologies Used
- Database - SQLite. Convenient for this challenge as nothing needs to be installed,
and configured to run in-memory for the tests.
- Cache - A memory cache is used for dev as nothing needs to be installed,
and a dummy cache is used for the tests.

## Running The Tests
```
pip install -r requirements.txt
pip install tox
tox
```

## Running The API
```
pip install -r requirements.txt
pip install -e .
dog_api.init
flask run
```

## A Note About Task 4.
- I am not using a cache that takes urls, but am using a different cache backend
for dev vs test.
- The Flask CLI is now used to start the development server, and port needs to be configured
outside of the Flask configuration system.
http://flask.pocoo.org/docs/1.0/cli/#run-the-development-server

## Production Considerations
- Pin dependencies to specific versions
- API documentation
- Use production grade database, cache and webserver.
- Authentication and authorization etc.
- Logging and performance monitoring
